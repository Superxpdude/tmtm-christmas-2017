// Task Descriptions
// Can be used for moving task descriptions from script files to configs


// Create new classes under here. BIS_fnc_taskCreate will use either the class passed as the description parameter (in string format)
// The following would be loaded by setting the description to "example";
// If the description is blank (""), then the task ID will be used automatically
class example
{
	title = "Example Task Title";					// Title of task. Displayed as the task name
	description = "Example Task Description";		// Description of task. Additional details displayed when the task is selected on the map screen.
	marker = "";									// Task marker. Leave blank
};
class presents
{
	title = "Deliver Presents";
	description = "Deliver presents to those on the 'Naughty List'.";
	marker = "";
};
class present1
{
	title = "Deliver First Present";
	description = "The first person on the 'Naughty List' forgot to hold the door for the guy behind him once. Use those 'Presents' that you have to blow up his house.";
	marker = "";
};
class present2
{
	title = "Deliver Second Present";
	description = "The second person on the 'Naughty List' forgot to say 'Thank You' after receiving his morning coffee yesterday. What a terrible person. Blow his house up too.";
	marker = "";
};
class present3	// Drugs crate
{
	title = "Deliver Third Present";
	description = "The third person on the 'Naughty List' is actually a drug trafficker. We don't know where he lives, but we know where he stores his goods. Blow up his drugs.";
	marker = "";
};
class present4
{
	title = "Deliver Fourth Present";
	description = "The fourth person on the Naught List is a landlord who owns an apartment block. His rental rates are always too high, so let's bring them down a bit. Blow up his apartment block.";
	marker = "";
};
class present5	// Pub
{
	title = "Deliver Fifth Present";
	description = "The fifth person runs a pub in downtown Chernogorsk, he got a noise complaint once from some nerd who moved in next door. We'll show that guy a real noise complaint. Blow up the pub.";
	marker = "";
};
class present6	// Hotel
{
	title = "Deliver Sixth Present";
	description = "The sixth person on the Naughty List owns the international hotel in Chernogorsk. He's never installed a wheelchair ramp to make his hotel wheelchair accessible. Blow up the hotel.";
	marker = "";
};
class present7	// Dock
{
	title = "Deliver Seventh Present";
	description = "This last guy has been really naughty this year. According to our reports, he never uses his turn signals. You know what to do.";
	marker = "";
};
class tanks
{
	title = "Obtain Mine Clearing Equipment";
	description = "Our recon elves have spotted some old soviet mine-clearing equipment in a shed to the north-east of Balota Airfield. You'll need them to reach Chernogorsk";
	marker = "";
};
class secretSanta
{
	title = "Recover Secret Santa Gifts";
	description = "Some of the non-believers have captured the truck carrying TMTM's secret santa gifts. Go get them back.";
	marker = "";
};
class saveChristmas1
{
	title = "Recover the Secret Santa delivery van";
	description = "The non-believers have taken our delivery van over to a coumpound near Prigorodki. Go get it back.";
	marker = "";
};
class saveChristmas2
{
	title = "Bring the van to the airfield";
	description = "One of our top-gun elves will come to pick up the van from the airfield. Take it there.";
	marker = "";
};