// Script to set up the command vehicle

if (!isServer) exitWith {};

_this params [
	["_newVehicle", objNull, [objNull]],
	["_oldVehicle", objNull, [objNull]]
];

_newVehicle addMPEventHandler ["MPKilled", {
	[] call SXP_fnc_respawnDisable;
	if (isServer) then {
		missionNamespace setVariable ["SXP_var_respawnDisabled", true, true];
	};
}];

if !(isNull _oldVehicle) then {
	[] remoteExec ["SXP_fnc_respawnEnable",0];	
	_newVehicle setVariable ["ACE_isRepairVehicle", true, true];
	missionNamespace setVariable ["SXP_var_respawnDisabled", false, true];
};